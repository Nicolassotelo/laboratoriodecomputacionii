package desafio8;

import java.util.Scanner;

public class RaizScanner {
	public static void main(String[] args) {
		double numero, raiz;

		System.out.println("INGRESE UN NUMERO...");
		Scanner entrada = new Scanner(System.in);
		numero = entrada.nextDouble();

		raiz = Math.sqrt(numero);
		
		System.out.println("La raiz cuadrada de "+numero+" es "+raiz);

	}
}