package desafio7;

public class Desaf7 {

	public static void main(String[] args) {
		
		
		double valor = 0;
		 
		double angulo = 90;
		double anguloRadianes = Math.toRadians(angulo);
		 
		valor = Math.cos(anguloRadianes);
		System.out.println("Coseno de " + angulo + "� = " + valor);
		 
		valor = Math.sin(anguloRadianes);
		System.out.println("Seno de " + angulo + "� = " + valor);
		 
		valor = Math.tan(anguloRadianes);
		System.out.println("Tangente de " + angulo + "� = " + valor);
		 
		valor = 0.707;
		 
		anguloRadianes = Math.acos(valor);
		angulo = Math.toDegrees(anguloRadianes);
		System.out.println("Arco Coseno de " + valor + " = " + angulo + "�");
		 
		anguloRadianes = Math.asin(valor);
		angulo = Math.toDegrees(anguloRadianes);
		System.out.println("Arco Seno de " + valor + " = " + angulo + "�");
		 
		anguloRadianes = Math.atan(valor);
		angulo = Math.toDegrees(anguloRadianes);
		System.out.println("Arco Tangente de " + valor + " = " + angulo + "�");
		
		
		System.out.println("exp(1.0) es " +  Math.exp(1.0));
	    System.out.println("exp(10.0) es " + Math.exp(10.0));
	    System.out.println("exp(0.0) es " +  Math.exp(0.0));

	    System.out.println("log(1.0) es " + Math.log(1.0));
	    System.out.println("log(10.0) es " + Math.log(10.0));
	    System.out.println("log(Math.E) es " + Math.log(Math.E));
		
		
		System.out.println("el numero e:"+Math.E);
		System.out.println("el numero pi:"+Math.PI);


}
}
